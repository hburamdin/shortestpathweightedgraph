This solution has following 3 projects
-->ComputerRetrievalConsole 
1. Console UI to give the ability for the user to select the route to calculate distance between route
2. Loads the data from the file path provided in config key ()
3. Loads the 3 letter route word(source&dest&distance) from file into tuples object
   "AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"
-->ShortestPathInWeightedGraphLib - This is core assembly which has FindPath(IEnumerable<string> route) calculates the distance
   ShortestPathInWeightedGraph object constructed with the provided tuples
   new WeightedDirectedGraph(List<Tuple<string, string, int>> nodesWithWeight)

   Client should call with multiple route names to get the distance of the route
   double FindPath(IEnumerable<string> route)
   
   2 nodes gives the shortest path between nodes
   More than 2 nodes as input gives the straight path along the nodes of the route.

-->ShortestPathInWeightedGraphLib.Tests
   This creates mock route tuples object and feeds data into WeightedDirectedGraph object.
   Used NUnit test and multiple testcases under the same test method.