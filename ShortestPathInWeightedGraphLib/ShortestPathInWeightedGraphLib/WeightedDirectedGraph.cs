﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShortestPathInWeightedGraphLib
{
    /// <summary>
    /// References the graph with edges and its weights
    /// </summary>
    public class WeightedDirectedGraph
    {
        private readonly HashSet<GraphNode> graphNodes;

        public WeightedDirectedGraph(List<Tuple<string, string, int>> nodesWithWeight)
        {
            graphNodes = new HashSet<GraphNode>();
            if (nodesWithWeight == null || nodesWithWeight.Any(s => string.IsNullOrEmpty(s.Item1)
            || string.IsNullOrEmpty(s.Item2)))
                return;

            foreach (var nodeW in nodesWithWeight)
            {
                var srcNode = graphNodes.FirstOrDefault(n => n.Name == nodeW.Item1);
                var destNode = graphNodes.FirstOrDefault(n => n.Name == nodeW.Item2);

                if (srcNode == null)
                {
                    srcNode = new GraphNode(nodeW.Item1.ToUpper());
                    graphNodes.Add(srcNode);
                }
                if (destNode == null)
                {
                    destNode = new GraphNode(nodeW.Item2.ToUpper());
                    graphNodes.Add(destNode);
                }

                srcNode.Edges.AddLast(new NodeEdge(destNode, nodeW.Item3));
            }
        }

        /// <summary>
        /// Finds the shortes path between the given nodes
        /// </summary>
        /// <param name="route">node collection to traverse</param>
        /// <returns>returns the path between nodes</returns>
        public double FindPath(IEnumerable<string> route)
        {
            if(route == null)
                throw new ArgumentNullException("route");

            var routedNodes = route.Select(s=>s.ToUpper()).ToList();
            if (route.Count() < 2)
                throw new ArgumentOutOfRangeException("route", "Minimum number of nodes should be 2");

            if (routedNodes.Count == 2)
                return FindShortestPath(routedNodes[0], routedNodes[1]);
            else
                return FindStraightPath(routedNodes);
        }

        private double FindStraightPath(List<string> routedNodes)
        {
            double routeLength = 0;
            var currentNodeName = routedNodes[0];
            for (int i = 1; i < routedNodes.Count; i++)
            {
                var nextNodeName = routedNodes[i];
                ValidateNodes(currentNodeName, nextNodeName);

                var edge = FindEdge(currentNodeName, nextNodeName);
                if (edge == null)
                    return -1;

                routeLength += edge.Weight;
                currentNodeName = nextNodeName;
            }

            return routeLength;
        }

        private double FindShortestPath(string source, string dest)
        {
            UnvisitAll();

            var nodesTuple = ValidateNodes(source, dest);
            var start = nodesTuple.Item1;
            var end = nodesTuple.Item2;

            var pathMap = InitGraphNodes();
            CalculateEdgeWeights(start.Edges, pathMap);

            while (true)
            {
                GraphNode currentNode = NearestReachableUnvisisted(pathMap);
                if (currentNode == null)
                    return -1;

                if (currentNode == end)
                    return pathMap[end];

                if (currentNode != start)
                    currentNode.Visit();
                foreach (NodeEdge edge in currentNode.Edges)
                {
                    if (edge.Destination.IsVisited())
                        continue;

                    var currentWeight = GetWeight(pathMap, currentNode);
                    var destWeight = GetWeight(pathMap, edge.Destination);
                    var newWeight = currentWeight + edge.Weight;
                    if (newWeight < destWeight)
                        pathMap[edge.Destination] = newWeight;
                }
            }
        }

        private void CalculateEdgeWeights(LinkedList<NodeEdge> edges, Dictionary<GraphNode, double> pathMap)
        {
            if (edges == null || pathMap == null) return;

            foreach (NodeEdge edge in edges)
            {
                if (pathMap.ContainsKey(edge.Destination))
                    pathMap[edge.Destination] = edge.Weight;
            }
        }

        private Dictionary<GraphNode, Double> InitGraphNodes()
        {
            var pathMap = new Dictionary<GraphNode, Double>();
            foreach (GraphNode node in graphNodes)
                pathMap.Add(node, Double.PositiveInfinity);

            return pathMap;
        }

        private Tuple<GraphNode, GraphNode> ValidateNodes(string source, string dest)
        {
            var start = graphNodes.FirstOrDefault(s => s.Name.ToLower() == source.ToLower());
            var end = graphNodes.FirstOrDefault(s => s.Name.ToLower() == dest.ToLower());
            if (start == null)
                throw new ArgumentException("Invalid source name '" + source + "'");
            if (end == null)
                throw new ArgumentException("Invalid destination name '" + dest + "'");

            return new Tuple<GraphNode, GraphNode>(start, end);
        }

        private double GetWeight(Dictionary<GraphNode, double> pathMap, GraphNode currentNode)
        {
            if (pathMap.ContainsKey(currentNode))
                return pathMap[currentNode];

            return 0.0;
        }

        private GraphNode NearestReachableUnvisisted(Dictionary<GraphNode, Double> pathMap)
        {
            double shortestDistance = Double.PositiveInfinity;
            GraphNode closestReachableNode = null;
            foreach (GraphNode node in graphNodes)
            {
                if (node.IsVisited())
                    continue;

                double currentDistance = pathMap[node];
                if (currentDistance == Double.PositiveInfinity)
                    continue;

                if (currentDistance < shortestDistance)
                {
                    shortestDistance = currentDistance;
                    closestReachableNode = node;
                }
            }
            return closestReachableNode;
        }

        private void UnvisitAll()
        {
            graphNodes.ToList().ForEach(s => s.UnVisit());
        }

        private NodeEdge FindEdge(string currentNodeName, string destinationNodeName)
        {
            var currentNode = graphNodes.FirstOrDefault(s => s.Name == currentNodeName);
            if (currentNode == null) return null;

            return currentNode.Edges.FirstOrDefault(s => s.Destination.Name == destinationNodeName);
        }
    }
}