﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortestPathInWeightedGraphLib
{
    internal class NodeEdge
    {
        private readonly GraphNode _destination;
        private readonly double _weight;

        internal NodeEdge(GraphNode dest, double w)
        {
            _destination = dest;
            _weight = w;
        }

        internal GraphNode Destination => _destination;

        internal double Weight => _weight;
    }
}
