﻿1. Client loading the data from file into tuples object
   3 letter word source&dest&distance for each data
   "AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"
   
2. ShortestPathInWeightedGraph object constructed with the provided tuples
   new WeightedDirectedGraph(List<Tuple<string, string, int>> nodesWithWeight)

3. Client should call with multiple route names to get the distance of the route
   double FindPath(IEnumerable<string> route)
   
   2 nodes gives the shortest path between nodes
   More than 2 nodes as input gives the straight path along the nodes of the route.
