﻿using System.Collections.Generic;

namespace ShortestPathInWeightedGraphLib
{
    internal class GraphNode
    {
        private readonly LinkedList<NodeEdge> _edges;

        private readonly string _name;
        private bool _visited;

        internal GraphNode(string name)
        {
            _name = name;
            _visited = false;
            _edges = new LinkedList<NodeEdge>();
        }

        internal string Name { get => _name; }

        internal LinkedList<NodeEdge> Edges => _edges;

        internal bool IsVisited()
        {
            return _visited;
        }

        internal void Visit()
        {
            _visited = true;
        }

        internal void UnVisit()
        {
            _visited = false;
        }

    }
}
