﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace ShortestPathInWeightedGraphLib.Tests
{
    [TestFixture]
    public class WeightedGraphTests
    {
        WeightedDirectedGraph graphWeighted;

        [OneTimeSetUp]
        public void Setup()
        {
            var routeNodes = new List<string>() { "AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7" };
            var tuples = new List<Tuple<string, string, int>>();
            foreach (var route in routeNodes)
            {
                var routeArray = route.ToCharArray();
                var source = Convert.ToString(routeArray[0]);
                var dest = Convert.ToString(routeArray[1]);
                var weight = Convert.ToString(routeArray[2]);

                tuples.Add(new Tuple<string, string, int>(source, dest, Convert.ToInt16(weight)));
            }
            graphWeighted = new WeightedDirectedGraph(tuples);
        }

        [TestCase("", "", 9)]
        [TestCase("Z", "F", 6)]
        [TestCase("", "A", 9)]
        [TestCase("A", "", 9)]
        public void ShortPathInvalidData(string source, string dest, double expectedDistance)
        {
            try
            {
                graphWeighted.FindPath(new List<string> { source, dest });
            }
            catch (ArgumentException aex)
            {
                Assert.Pass();
                return;
            }

            Assert.Fail("Invalid data not handled.");
        }

        [TestCase("A", "C", 9)]
        [TestCase("B", "E", 6)]
        [TestCase("B", "B", 9)]
        [TestCase("C", "C", 9)]
        public void ShortPathBetweenTwoNodes(string source, string dest,
            double expectedDistance)
        {
            var actualDistance = graphWeighted.FindPath(new List<string> { source, dest });
            Assert.AreEqual(expectedDistance, actualDistance);
        }


        [TestCase(new string[] { "A", "B", "C" }, 9)]
        [TestCase(new string[] { "A", "E", "D" }, -1)]
        [TestCase(new string[] { "A", "E", "B", "C", "D" }, 22)]
        public void ShortPathBetweenNodes(string[] route, double expectedDistance)
        {
            var distance = graphWeighted.FindPath(route);
            Assert.AreEqual(expectedDistance, distance);
        }
    }
}
