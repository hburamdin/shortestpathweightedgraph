﻿using ShortestPathInWeightedGraphLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerRetrievalConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var routeNodes = LoadRoutes();
            var tuples = new List<Tuple<string, string, int>>();
            foreach (var route in routeNodes)
            {
                var routeArray = route.ToCharArray();
                var source = Convert.ToString(routeArray[0]);
                var dest = Convert.ToString(routeArray[1]);
                var weight = Convert.ToString(routeArray[2]);

                tuples.Add(new Tuple<string, string, int>(source, dest, Convert.ToInt16(weight)));
            }

            WeightedDirectedGraph graphWeighted = new WeightedDirectedGraph(tuples);
            Console.WriteLine(routeNodes.ToString());
            Console.WriteLine("Routes-> " + routeNodes.ToCSV<string>());
            Console.WriteLine("Enter Route (A,C for straight path and A,E,D,...for short path): ");
            Console.WriteLine("Type exit to stop.");

            while (true)
            {
                Console.WriteLine("Enter Route: ");
                var vals = Console.ReadLine();
                if (vals == "exit")
                    break;

                var splitNodes = vals.Split(',');
                try
                {
                    Console.WriteLine(graphWeighted.FindPath(splitNodes));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.");
                }
            }
        }

        static List<string> LoadRoutes()
        {
            var filePath = System.Configuration.ConfigurationManager.AppSettings["RouteFile"];
            if (!File.Exists(filePath))
                throw new FileNotFoundException("Invalid file path.");

            var allText = File.ReadAllText(filePath);
            var nodes = allText.Split(',').ToList();
            if (nodes.Any(s => s.Length != 3))
                throw new InvalidDataException("Invalid route data.");

            return nodes;
        }
    }

    public static class Extensions
    {
        public static string ToCSV<T>(this IEnumerable<T> value)
        {
            if (value == null)
                return string.Empty;

            var b = new System.Text.StringBuilder(60);
            var e = value.GetEnumerator();
            if (e.MoveNext())
                b.Append(e.Current.ToString());

            while (e.MoveNext())
            {
                b.Append(',');
                b.Append(e.Current.ToString());
            }

            return b.ToString();
        }
    }
}
